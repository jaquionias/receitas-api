
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('categories').del()
    .then(function () {
      // Inserts seed entries
      return knex('categories').insert([
        {name: 'Bolos e tortas doces'},
        {name: 'Carnes'},
        {name: 'Aves'},
        {name: 'Peixes e frutos do mar'},
        {name: 'Saladas, molhos e acompanhamentos'},
        {name: 'Sopas'},
        {name: 'Massas'},
        {name: 'Bebidas'},
        {name: 'Doces e sobremesas'},
        {name: 'Lanches'},
        {name: 'Prato Único'},
        {name: 'Light'},
        {name: 'Alimentação Saudável'},
      ]);
    });
};
