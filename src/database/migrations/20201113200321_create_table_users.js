exports.up = function (knex) {
    return knex.schema.createTable('users', function (table) {
        table.increments('id').primary();
        table.string('name', 100);
        table.string('login', 100).notNullable();
        table.string('password', 100).notNullable();
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('current_timestamp on update current_timestamp'));
        table.timestamp("deleted_at").nullable();

    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('users');
};
