exports.up = function (knex) {
    return knex.schema.createTable('recipes', function (table) {
        table.increments('id').primary();
        table.integer('id_user').unsigned().references('id').inTable('users');
        table.integer('id_category').unsigned().references('id').inTable('categories')
        table.string('name', 45);
        table.integer('preparation_time_minutes');
        table.integer('wells');
        table.text('preparation_mode').notNullable();
        table.text('ingredients');
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('current_timestamp on update current_timestamp'));
        table.timestamp("deleted_at").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('recipes');
};
