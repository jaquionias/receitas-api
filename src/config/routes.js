const express = require('express');
const authenticate = require('./passport');

const auth = require('../api/auth');
const users = require('../api/user');
const recipes = require('../api/recipes');
const categories = require('../api/categories');

const routes = express.Router();

routes.post('/signup', users.store);
routes.post('/signin', auth.signin);
routes.post('/validateToken', auth.validateToken);

//Rotas do usuário
routes.route('/userss')
    .post(users.store)
    .all(authenticate.authenticate())
    .get(users.getAll);
routes.route('/userss/:id')
    .all(authenticate.authenticate())
    .get(users.getById)
    .delete(users.destroy);


//Rotas das receitas
routes.route('/recipes')
    .all(authenticate.authenticate())
    .post(recipes.store)
    .get(recipes.getAll);
routes.route('/recipes/:id')
    .all(authenticate.authenticate())
    .get(recipes.getById)
    .put(recipes.update)
    .delete(recipes.destroy);

//Rotas das categories
routes.route('/categories')
    .all(authenticate.authenticate())
    .get(categories.getAll);

module.exports = routes;