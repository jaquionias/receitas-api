const connection = require('../database/connection');

module.exports = {
    getAll(request, response) {
        connection('categories')
            .then(categories => response.json(categories))
            .catch(err => response.status(500).send(err));
    },

};