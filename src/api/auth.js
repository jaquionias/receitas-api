const { authSecret } = require('../../.env');
const jwt = require('jwt-simple');
const bcrypt = require('bcrypt');
const connection = require('../database/connection');

module.exports = {
    async signin(request, response) {
        if (!request.body.login || !request.body.password) {
            return response.status(400).json({message: 'Informe o usário e a senha'});
        }

        const user = await connection('users')
            .where('login', request.body.login).first();
        if (!user) {
            return response.status(400).json({message: 'Usuário não encontrado!'});
        }

        const isMatch = bcrypt.compareSync(request.body.password, user.password);

        if (!isMatch) {
            return response.status(401).json({message: 'Login/senha inválidos!'});
        }

        const now = Math.floor(Date.now() / 1000);

        const payload = {
            id: user.id,
            name: user.name,
            login: user.login,
            iat: now,
            exp: now + (60 * 60 * 24),
        };

        return response.json({
            ...payload,
            token: jwt.encode(payload, authSecret),
        });
    },

    async validateToken(request, response) {
        const userData = request.body || null;

        try {
            if (userData) {
                const token = jwt.decode(userData.token, authSecret);

                if (new Date(token.exp * 1000) > new Date()) {
                    return response.send(true);
                }
            }
        } catch (e) {
            //problema no token
        }

        response.send(false);
    },
};