const connection = require('../database/connection');
const { existsOrError, notExistsOrError } = require('./helpers/validation');

module.exports = {
    async store(request, response) {
        const recipe = { ...request.body };

        try {
            existsOrError(recipe.id_user, 'Você precisa logar para cadastrar uma receira');
            existsOrError(recipe.preparation_mode, 'Modo de preparo é obrigatório');
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }

        connection('recipes')
            .insert(recipe)
            .then(_ => response.status(204).send('Receita cadastrada com sucesso!'))
            .catch(err => response.status(500).send(err));
    },
    async update(request, response) {
        const recipe = { ...request.body };
        const id = request.params.id;
        delete recipe.id;
        delete recipe.created_at;
        recipe.updated_at = new Date();

        try {
            existsOrError(recipe.id_user, 'Você precisa logar para cadastrar uma receira');
            existsOrError(recipe.preparation_mode, 'Modo de preparo é obrigatório');
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }

        connection('recipes')
            .update(recipe)
            .where('id', id)
            .whereNull('deleted_at')
            .then(_ => response.status(204).send('Receita atualizada com sucesso!'))
            .catch(err => response.status(500).send(err));
    },

    getAll(request, response) {
        const dataSearch = request.query.search;
        const id_user = request.query.id_user;

        connection('recipes')
            .whereNull('deleted_at')
            .where('id_user', id_user)
            .where('name', 'like', `%${dataSearch}%`)
            .then(users => response.json(users))
            .catch(err => response.status(500).send(err));

    },
    async getById(request, response) {
        try {
            const recipe = await connection('recipes')
                .where('id', request.params.id)
                .whereNull('deleted_at')
                .first();
            existsOrError(recipe, 'Receira não encontrado!');

            response.status(200).json(recipe);
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }

    },


    async destroy(request, response) {
        try {

            const rowsUpdated = await connection('recipes')
                .update({ 'deleted_at': new Date() })
                .where('id', request.params.id)
                .whereNull('deleted_at');
            existsOrError(rowsUpdated, 'Receira não encontrado!');

            response.status(204).send();
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }
    },
};