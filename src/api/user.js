const bcrypt = require('bcrypt');
const connection = require('../database/connection');
const { existsOrError, equalsOrError, notExistsOrError } = require('./helpers/validation');

module.exports = {
    async store(request, response) {
        const user = { ...request.body };


        try {
            existsOrError(user.login, 'Login não informado!');
            existsOrError(user.password, 'Senha não informada');
            equalsOrError(user.password, user.confirmPassword, 'As senhas informadas são diferentes!');

            const loginFromDB = await connection('users')
                .where('login', user.login).first();
            notExistsOrError(loginFromDB, 'O Login informado já existe no banco de dados, informe outro!');
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }

        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(user.password, salt);

        delete user.confirmPassword;

        connection('users')
            .insert(user)
            .then(_ => response.status(204).send('Usuário cadastrado com sucesso!'))
            .catch(err => response.status(500).send(err));
    },

    getAll(request, response) {
        connection('users')
            .whereNull('deleted_at')
            .then(users => response.json(users))
            .catch(err => response.status(500).send(err));
    },
    getById(request, response) {
        connection('users')
            .where('id', request.params.id)
            .whereNull('deleted_at')
            .first()
            .then(users => response.json(users))
            .catch(err => response.status(500).send(err));
    },

    async destroy(request, response) {
        try {
            const recipes = await connection('recipes')
                .where('id_user', request.params.id).first();
            notExistsOrError(recipes, 'O usuário possui receitas cadastradas!');

            const rowsUpdated = await connection('users')
                .update({ 'deleted_at': new Date() })
                .where('id', request.params.id)
                .whereNull('deleted_at');
            existsOrError(rowsUpdated, 'Usuário não encontrado!');

            response.status(204).send();
        } catch (msg) {
            return response.status(400).json({ message: msg });
        }
    },
};