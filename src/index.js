//Configuração do express para rotas e reconhecimento de JSON
const express = require('express');
const routes = require('./config/routes');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

//Porta que a API irá escutar.
const PORT = 3333;

//Inicialiação do API
app.listen(PORT, function () {
    console.log("API rodando na porta " + PORT);
});