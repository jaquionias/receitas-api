# api

## Setup do projeto
```
npm install
```

### Configurar .env

```
cp .env_exemple .env
```

### Criar um banco de dados Mysql localmente
#### Dê preferência ao nome "culinary_recipes"

### Rodar migrations

```
npx knex migrate:latest
```

###### Caso dê algum erro: Invalid default value for 'deleted_at'
###### São as políticas do Mysql, para corrigir, em seu banco de dados, rode:

```
SET GLOBAL sql_mode = 'ALLOW_INVALID_DATES';
SET SESSION sql_mode = 'ALLOW_INVALID_DATES';
```

###### Depois

```
npx knex migrate:rollback
```

### Rodar migrations novamente

```
npx knex migrate:latest
```

### Rodar seeds para categoria

```
npx knex seed:run
```

### Iniciar api
```
node src/index.js
```
