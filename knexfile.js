// Update with your config settings.
const config = require('./.env');
module.exports = {

    client: 'mysql',
    connection: {
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database,
    },
    pool: {
        min: 2,
        max: 10,
    },
    migrations: {
        directory: './src/database/migrations',
    },
    seeds: {
        directory: './src/database/seeds',
    }

};
